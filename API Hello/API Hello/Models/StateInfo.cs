﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Hello.Models
{
    public class StateInfo
    {
        String name { get; set; }
        String codeIBGE { get; set };
        String areaKM { get; set; }
    }
}