﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft;
using Newtonsoft.Json;

namespace API_Hello.Models
{
    public class Dados
    {
        public string bairro { get; set; }
        public string cidade { get; set; }
        public EstadoInfo estado_info { get; set; }
        public string cep { get; set; }
        public CidadeInfo cidade_info { get; set; }
        public string estado { get; set; }
        public string logradouro { get; set; }

        public void Carregar(String arquivo)
        {
           Dados  dados= JsonConvert.DeserializeObject<Dados>(arquivo);
            bairro = dados.bairro;
            cidade = dados.cidade;
            cep = dados.cep;
            estado = dados.estado;
            cidade=dados.cidade;
            logradouro = dados.logradouro;
            
        }
    }
    
}