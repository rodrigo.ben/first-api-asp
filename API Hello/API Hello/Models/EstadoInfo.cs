﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_Hello.Models
{
    public class EstadoInfo
    {
        public string area_km2 { get; set; }
        public string codigo_ibge { get; set; }
        public string nome { get; set; }
    }
}